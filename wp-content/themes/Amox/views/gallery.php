<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$videos = $fields['video_item'];
$published_posts = count($videos);
?>

<article class="article-page-body page-body">
	<div class="title-wrap">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-auto">
					<h1 class="block-title">
						<?php the_title(); ?>
					</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid pt-2 mb-4">
		<div class="row justify-content-center">
			<div class="col-xl-11 col-12">
				<div class="row justify-content-center align-items-start mb-3">
					<div class="col-xl col-12 breadcrumbs-custom">
						<?php if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
						} ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="posts-page">
		<?php if ($videos) : ?>
			<div class="posts-output">
				<div class="container">
					<div class="row justify-content-center align-items-stretch put-here-posts">
						<?php foreach ($videos as $i => $video) : ?>
							<?php if ($i < 6) : get_template_part('views/partials/card', 'video', [
									'video' => $video,
							]); endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<div class="video-modal">
				<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
					 aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-body" id="iframe-wrapper"></div>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true" class="close-icon">×</span>
							</button>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if ($published_posts && $published_posts > 6) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-content="video" data-page="<?= get_queried_object_id(); ?>">
						<?= lang_text(['he' => 'עוד סרטונים', 'en' => 'More videos'], 'he'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<section class="repeat-form-post mt-5">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</section>
<?php if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'block_text' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>

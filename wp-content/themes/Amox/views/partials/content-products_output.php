<section class="products-output m-50">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<?php if (isset($args['block_title']) && $args['block_title']) : ?>
					<h2 class="base-title"><?= $args['block_title']; ?></h2>
				<?php endif; ?>
			</div>
		</div>
		<?php if (isset($args['products']) && $args['products']) : ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($args['products'] as $_p){
					echo '<div class="col-lg-3 col-sm-6 product-col">';
					setup_postdata($GLOBALS['post'] =& $_p);
					wc_get_template_part( 'content', 'product' );
					echo '</div>';
				}
				wp_reset_postdata(); ?>
			</div>
		<?php endif;
		if (isset($args['block_link']) && $args['block_link']) : ?>
			<div class="row justify-content-end mt-3">
				<div class="col-auto">
					<a href="<?= $args['block_link']['url']; ?>" class="block-link">
						<?= (isset($args['block_link']['title']) && $args['block_link']['title'])  ? $args['block_link']['title'] : 'לכל המוצרים'; ?>
					</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>

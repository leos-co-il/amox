<?php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$regular_price = $product->get_regular_price();
$sale_price = $product->is_on_sale() ? $product->get_sale_price() : null;
$currency_symbol = get_woocommerce_currency_symbol();

?>

<span class="price">
	<span class="price-title mx-1">
		<?= lang_text(['he' => 'מחיר: ', 'en' => 'Price:'], 'he')?>
	</span>
	<?php if($sale_price): ?>
	<del>
		<span class="woocommerce-Price-amount amount mx-2">
			<bdi>
				<span class="woocommerce-Price-currencySymbol"><?= $currency_symbol ?></span>
				<?= $regular_price ?>
			</bdi>
		</span>
	</del>
	<?php endif; ?>
	<ins>
		<span class="woocommerce-Price-amount amount">
			<bdi>
				<span class="woocommerce-Price-currencySymbol"><?= $currency_symbol ?></span>
				<?= $sale_price ? $sale_price : $regular_price ?>
			</bdi>
		</span>
	</ins>
</span>




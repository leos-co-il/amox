<?php
/**
 * Single Product Up-Sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/up-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $upsells ) : ?>

	<section class="up-sells upsells products">
		<div class="container upsells-cont">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="logo-title-wrap">
						<h2 class="block-title">
							<?= lang_text(['he' => 'הוסיפו מעמד למיכל שלכם', 'en' => 'Add a stand to your container'], 'he'); ?>
						</h2>
					</div>
				</div>
			</div>
			<?php woocommerce_product_loop_start(); ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ( $upsells as $upsell ) : ?>
					<div class="col-upsells col-lg-4 col-sm-6 col-12 mb-5">
						<?php
						$post_object = get_post( $upsell->get_id() );

						setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

						wc_get_template_part( 'content', 'product' );
						?>
					</div>
				<?php endforeach; ?>
			</div>
			<?php woocommerce_product_loop_end(); ?>
		</div>
	</section>

	<?php
endif;

wp_reset_postdata();

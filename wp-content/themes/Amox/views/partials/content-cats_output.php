<?php if (isset($args['cats']) && $args['cats']) : ?>
<section class="home-cats-block">
	<?php foreach ($args['cats'] as $i => $cat_item) :
		$thumbnail_id = get_term_meta($cat_item->term_id, 'thumbnail_id', true );
		$cat_image = wp_get_attachment_url( $thumbnail_id ); ?>
		<div class="home-category wow zoomIn" data-wow-delay="0.<?= $i * 2; ?>s"
			<?php if ($cat_image) : ?>
				style="background-image: url('<?= $cat_image; ?>')"
			<?php endif; ?>>
			<div class="home-category-overlay">
				<a class="base-link home-category-link" href="<?= get_category_link($cat_item); ?>">
					<?= $cat_item->name; ?>
				</a>
				<?php if ($cat_img_field = get_field('cat_img', $cat_item)) : ?>
					<div class="cat-img-custom">
						<img src="<?= $cat_img_field['url']; ?>" alt="category-image">
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endforeach; ?>
</section>
<?php endif; ?>

<?php

if(!isset($args['products']))
	return;

?>

<div class="row justify-content-center align-items-stretch">
	<?php

	foreach ($args['products'] as $_p){
		echo '<div class="col-xl-4 col-lg-6 col-md-12 col-sm-6 col-12 mb-4 product-item-col">';
		setup_postdata($GLOBALS['post'] =& $_p);
		wc_get_template_part( 'content', 'product' );
		echo '</div>';
	}
	wp_reset_postdata();

	?>
</div>

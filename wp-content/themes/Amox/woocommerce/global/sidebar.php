<?php
/**
 * Sidebar
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/sidebar.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $wp_query;
$query_cat = [];

$terms = get_terms( [
		'taxonomy' => 'product_cat',
		'hide_empty' => false,
] );
$query_curr = get_queried_object();
?>

<div id="sidebar" class="mb-4">
	<div class="sidebar-box">
		<div class="box-content">
			<ul class="box-list">
				<?php foreach ($terms as $_data_item): ?>
					<li>
						<a href="<?= get_term_link($_data_item)?>" class="filter-link
<?= ($query_curr->term_id === $_data_item->term_id) ? 'curr-link' : ''; ?>">
							<?= $_data_item->name ?>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>

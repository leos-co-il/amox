<?php
$facebook = opt('facebook');
?>

<footer>
	<div class="footer-main">
		<a id="go-top">
			<img src="<?= ICONS ?>to-top.png" alt="to-top" class="to-top-img">
			<h5 class="to-top-text">
				<?= lang_text(['he' => 'חזרה </br> למעלה', 'en' => 'To top'], 'he'); ?>
			</h5>
		</a>
		<div class="container footer-container-menu">
			<div class="row justify-content-center align-items-center mb-3">
				<div class="col-xl-8 col-lg-9 col-sm-11 col-12 d-flex flex-column align-items-center justify-content-center">
					<?php if ($logo = opt('logo')) : ?>
						<div class="row justify-content-center w-100">
							<div class="col-8">
								<a href="/" class="logo-foo">
									<img src="<?= $logo['url'] ?>" alt="logo" class="w-100">
								</a>
							</div>
						</div>
					<?php endif; ?>
					<div class="row justify-content-center align-items-center mb-3">
						<div class="col-lg-auto">
							<?php if ($base_form_title = opt('foo_form_title')) : ?>
								<h2 class="foo-form-title"><?= $base_form_title; ?></h2>
							<?php endif; ?>
						</div>
						<div class="col-auto">
							<?php if ($base_form_subtitle = opt('foo_form_subtitle')) : ?>
								<h3 class="foo-form-text"><?= $base_form_subtitle; ?></h3>
							<?php endif; ?>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-12">
							<div class="middle-form wow zoomIn">
								<?php getForm('274'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row justify-content-between align-items-stretch">
				<div class="col-lg-auto col-md-6 col-12 foo-menu">
					<h3 class="foo-title">
						<?= lang_text(['he' => 'ניווט מהיר', 'en' => 'Fast navigation'], 'he'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '2'); ?>
					</div>
				</div>
				<div class="col-lg col-md-6 col-12 foo-menu foo-links-menu">
					<?php if ($foo_l_title_1 = opt('foo_menu_title_1')) : ?>
						<h3 class="foo-title">
							<?= $foo_l_title_1; ?>
						</h3>
					<?php endif; ?>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu-1', '1', 'hop-hey two-columns'); ?>
					</div>
				</div>
				<div class="col-lg col-md-6 col-12 foo-menu foo-links-menu">
					<?php if ($foo_l_title_2 = opt('foo_menu_title_2')) : ?>
						<h3 class="foo-title">
							<?= $foo_l_title_2; ?>
						</h3>
					<?php endif; ?>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu-2', '1', 'hop-hey two-columns'); ?>
					</div>
				</div>
				<?php if ($facebook) : ?>
					<div class="col-xl-auto col-md-6 facebook-widget foo-menu d-flex flex-column justify-content-end">
						<div class="facebook-title">
							<h3 class="foo-title">
								<?= lang_text(['he' => 'עקבו אחרינו', 'en' => 'Follow us'], 'he'); ?>
							</h3>
						</div>
						<div class="menu-border-top">
							<iframe src="https://www.facebook.com/plugins/page.php?href=<?= $facebook; ?>&tabs=timeline&width=300px&height=200px&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId"
									width="250px" height="150px" style="border:none;overflow:hidden" scrolling="no"
									frameborder="0" allowTransparency="true" allow="encrypted-media">
							</iframe>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php if ($pay_img = opt('payments')) : ?>
		<div class="pay-row">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-xl-11 col-12 pay-col">
						<?php foreach ($pay_img as $img) : ?>
							<img src="<?= $img['url']; ?>">
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>
<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>

<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$open_hours = opt('open_hours');
?>

<section class="page-body contact-page-body">
	<div class="title-wrap">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-auto">
					<h1 class="block-title"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row justify-content-center align-items-start mb-3">
			<div class="col-xl col-12 breadcrumbs-custom">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				} ?>
			</div>
		</div>
		<div class="row justify-content-center mt-5">
			<div class="col-xl-10 col-sm-11 col-12">
				<div class="row justify-content-center">
					<?php if ($open_hours) : ?>
						<div class="col-lg-4 col-sm-10 col-12 col-contact">
							<div class="contact-item">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-hours.png">
								</div>
								<div class="contact-info">
									<h3 class="contact-type-title">
										<?= lang_text(['he' => 'שעות פתיחה:', 'en' => 'Opening hours:'], 'he'); ?>
									</h3>
									<p class="contact-type"><?= $open_hours; ?></p>
								</div>
							</div>
						</div>
					<?php endif;
					if ($mail) : ?>
						<div class="col-lg-4 col-sm-10 col-12 col-contact">
							<a href="mailto:<?= $mail; ?>" class="contact-item">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-mail.png">
								</div>
								<div class="contact-info">
									<h3 class="contact-type-title">
										<?= lang_text(['he' => 'מייל ליצירת קשר:', 'en' => 'Contact email:'], 'he'); ?>
									</h3>
									<p class="contact-type"><?= $mail; ?></p>
								</div>
							</a>
						</div>
					<?php endif;
					if ($tel) : ?>
						<div class="col-lg-4 col-sm-10 col-12 col-contact">
							<a href="tel:<?= $tel; ?>" class="contact-item">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-tel.png">
								</div>
								<div class="contact-info">
									<h3 class="contact-type-title">
										<?= lang_text(['he' => 'טלפון ליצירת קשר:', 'en' => 'Contact phone number:'], 'he'); ?>
									</h3>
									<p class="contact-type"><?= $tel; ?></p>
								</div>
							</a></div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="row form-contact-row justify-content-center position-relative">
			<img src="<?= IMG ?>contact-img-oil.png" class="contact-img-oil" alt="contact-img-oil">
			<div class="col-xl-10 col-sm-11 col-12">
				<div class="contact-form-wrap mt-3 wow zoomIn" data-wow-delay="0.4s">
					<img src="<?= IMG ?>form-olive.png" class="olive-form" alt="olive">
					<div class="row justify-content-center align-items-center">
						<?php if ($fields['contact_form_title']) : ?>
							<div class="col-auto">
								<h2 class="pop-form-title form-title"><?= $fields['contact_form_title']; ?></h2>
							</div>
						<?php endif;
						if ($fields['contact_form_subtitle']) : ?>
							<div class="col-auto">
								<h3 class="pop-form-subtitle"><?= $fields['contact_form_subtitle']; ?></h3>
							</div>
						<?php endif; ?>
					</div>
					<div class="contact-page-form">
						<?php getForm('276'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>

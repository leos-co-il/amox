<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-lg-4 col-md-6 col-sm-10 col-12 mb-4 col-post">
		<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
			<div class="post-img"<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif; ?>>
					<a class="post-img-overlay" href="<?= $link; ?>">
						<h3 class="post-card-title"><?= $args['post']->post_title; ?></h3>
					</a>
			</div>
			<div class="post-card-content">
				<p class="base-post-text mb-3">
					<?= text_preview($args['post']->post_content, 25); ?>
				</p>
				<a class="block-link post-link" href="<?= $link; ?>">
					<?= lang_text(['he' => 'להמשך קריאה', 'en' => 'Continue reading'], 'he'); ?>
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>

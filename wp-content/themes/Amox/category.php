<?php
get_header();
$query = get_queried_object();
$posts = new WP_Query([
	'posts_per_page' => 6,
	'post_type' => 'post',
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
$published_posts = get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
?>
<article class="article-page-body page-body">
	<div class="title-wrap">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-auto">
					<h1 class="block-title">
						<?= $query->name; ?>
					</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid pt-2 mb-4">
		<div class="row justify-content-center">
			<div class="col-xl-11 col-12">
				<div class="row justify-content-center align-items-start mb-3">
					<div class="col-xl col-12 breadcrumbs-custom">
						<?php if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
						} ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12">
				<div class="base-output text-center">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="posts-page">
		<?php if ($posts->have_posts()) : ?>
			<div class="posts-output">
				<div class="container">
					<div class="row justify-content-center align-items-stretch put-here-posts">
						<?php foreach ($posts->posts as $i => $post) {
							get_template_part('views/partials/card', 'post', [
								'post' => $post,
							]); }
						?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if ($published_posts && $published_posts > 6) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-term="<?= $query->term_id; ?>">
						<?= lang_text(['he' => 'עוד מאמרים', 'en' => 'More posts'], 'he'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<section class="repeat-form-post mt-5">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</section>
<?php if ($faq = get_field('faq_item', $query)) :
	get_template_part('views/partials/content', 'faq',
		[
			'block_text' => get_field('faq_title', $query),
			'faq' => $faq,
		]);
endif;
get_footer(); ?>

<?php if (isset($args['video']) && $args['video']) :
	if (isset($args['video']['video_link']) && $args['video']['video_link']) : ?>
	<div class="col-lg-4 col-md-6 col-sm-10 col-12 mb-4 col-post">
		<div class="post-card more-card" data-id="<?= getYoutubeId($args['video']['video_link']); ?>">
			<div class="post-img" style="background-image: url('<?= getYoutubeThumb($args['video']['video_link']); ?>')">
				<div class="post-img-overlay video-img-overlay play-button" data-video="<?= getYoutubeId($args['video']['video_link']); ?>">
					<img src="<?= ICONS ?>play.png" alt="play-video">
				</div>
			</div>
			<div class="post-card-content align-items-center">
				<p class="video-title">
					<?= isset($args['video']['video_title']) && $args['video']['video_title'] ? $args['video']['video_title'] : ''; ?>
				</p>
			</div>
		</div>
	</div>
<?php endif; endif; ?>

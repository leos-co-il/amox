<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
?>

<article class="article-page-body page-body process-page-back">
	<div class="title-wrap">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-auto">
					<div class="logo-title-wrap mb-0">
						<h2 class="block-title">
							<?php the_title(); ?>
						</h2>
						<?php if ($logo = opt('logo_dark')) : ?>
							<img src="<?= $logo['url'] ?>" alt="logo" class="logo-title">
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid pt-5 mb-5">
		<div class="row justify-content-center">
			<div class="col-xl-11 col-12">
				<div class="row justify-content-center align-items-start mb-3">
					<div class="col-xl col-12 breadcrumbs-custom">
						<?php if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
						} ?>
					</div>
				</div>
				<div class="row justify-content-between">
					<div class="<?= has_post_thumbnail() ? 'col-xl-6 col-lg-7 col-12' : 'col-12'; ?>">
						<div class="base-output">
							<?php the_content(); ?>
						</div>
					</div>
					<?php if (has_post_thumbnail()) : ?>
						<div class="col-xl-5 col-lg-5 col-12">
							<img src="<?= postThumb(); ?>" alt="about-page-image" class="w-100">
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
if ($fields['about_cats']) {
	get_template_part('views/partials/content', 'cats_output',
			[
					'cats' => $fields['about_cats'],
			]);
}
if ($slider = $fields['single_slider_seo']) : ?>
	<div class="reverse-slider">
		<?php get_template_part('views/partials/content', 'slider',[
			'img' => $fields['slider_img'],
			'content' => $slider,
		]);?>
	</div>
<?php endif;
get_template_part('views/partials/repeat', 'form');
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'block_text' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>

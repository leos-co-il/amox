<div class="repeat-form-wrap">
	<img src="<?= IMG ?>contact-img-oil.png" class="form-oil" alt="contact-img-oil">
	<div class="repeat-form">
		<div class="container">
			<div class="row justify-content-center align-items-center mb-3">
				<?php if ($f_title = opt('base_form_title')) : ?>
					<div class="col-auto">
						<h2 class="pop-form-title form-title"><?= $f_title; ?></h2>
					</div>
				<?php endif;
				if ($f_subtitle = opt('base_form_subtitle')) : ?>
					<div class="col-auto">
						<h3 class="pop-form-subtitle"><?= $f_subtitle; ?></h3>
					</div>
				<?php endif; ?>
			</div>
			<div class="row">
				<div class="col-12">
					<?php getForm('273'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

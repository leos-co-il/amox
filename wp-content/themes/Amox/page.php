<?php

the_post();
get_header();
$fields = get_fields();

?>
<div class="container mt-5">
	<div class="row">
		<div class="col-12">
			<?php the_content(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>

<?php
defined('ABSPATH') or die("No script kiddies please!");
//require_once('inc/bs-walker.php');
require_once 'inc/helper.php';
require_once "inc/ajax_calls.php";
define('THEMEDIR', get_template_directory_uri());
define('THEMEPATH', get_template_directory());
define('PARTIALS', THEMEPATH . '/views/partials/');
define('STYLES', THEMEDIR . '/assets/styles/');
define('SCRIPTS', THEMEDIR . '/assets/scripts/');
define('IMG', THEMEDIR . '/assets/img/');
define('ICONS', THEMEDIR . '/assets/iconsall/');
define('PLUGINS', THEMEDIR . '/assets/plugins/');
define('NODE', THEMEDIR . '/node_modules/');
define('CS_PAGE_ASSETS', THEMEDIR . '/assets/coming-soon/');
define('CATALOG', false);
define('PROJECTS', false);
define('ENV', get_option('stage'));
define('TEXTDOMAIN', 'leos');
define('LANG', get_lang());


// ******* SETUP *******
load_theme_textdomain(TEXTDOMAIN, get_template_directory() . '/languages');

add_filter('show_admin_bar', '__return_false');
function amox_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'amox_add_woocommerce_support' );

function disable_wp_emojicons($plugins){
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}

function filter_site_upload_size_limit( $size ) {
    $size = 1024 * 10000;
    return $size;
}

add_filter( 'upload_size_limit', 'filter_site_upload_size_limit', 20 );

function wpcontent_svg_mime_type( $mimes = array() ) {
    $mimes['svg']  = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';
    return $mimes;
}
add_filter( 'upload_mimes', 'wpcontent_svg_mime_type' );

add_action('init', 'disable_wp_emojicons');

//Images sizes
add_image_size('gallery-thumb', 134, 94);


function scriptsInit()
{

    wp_enqueue_style('bootstrap', STYLES . 'bootstrap_app.css', false, 1.0);
    wp_enqueue_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array('jquery'), 1.0, false);
    wp_enqueue_script('bootstrap', NODE . 'bootstrap/dist/js/bootstrap.min.js', array('jquery'), 1.0, false);


    wp_enqueue_style('slick-css', PLUGINS . 'slick/slick.css', false, 1.0);
    wp_enqueue_style('slick-theme-css', PLUGINS . 'slick/slick-theme.css', false, 1.0);
    wp_enqueue_script('slick-js', PLUGINS . 'slick/slick.min.js', array('jquery'), 1.0, true);

    wp_enqueue_style('lightbox-css', PLUGINS . 'lightbox/src/css/lightbox.css', false, 1.0);
    wp_enqueue_script('lightbox-js', PLUGINS . 'lightbox/src/js/lightbox.js', array('jquery'), 1.0, true);

    wp_enqueue_style('app-style', STYLES . 'styles.css', false, 1.0);
	$object = array(
		'wp_ajax' => admin_url( 'admin-ajax.php' ),
	);
	wp_enqueue_script('mustache', NODE . 'mustache/mustache.min.js', array('jquery'), 1.0, true);


    wp_enqueue_script('app-scripts');
    wp_localize_script('app-scripts', 'JSObject', $object);
    wp_enqueue_script('app-scripts-setup', SCRIPTS . 'setup.js', array('jquery'), 1.0, true);
    wp_enqueue_script('app-scripts', SCRIPTS . 'scripts.js', array('jquery'), 1.0, true);

    wp_enqueue_script('wow', NODE . 'wow.js/dist/wow.min.js', array('jquery'), 1.0, false);

    wp_enqueue_style('primary-style', get_stylesheet_uri(), '', '1.0');
	if ((LANG !== NULL) && (LANG !== 'he')) {
		wp_enqueue_style('ltr-style', STYLES. 'ltr.css', false, 1.0);
	}
    //wp_enqueue_script('scroll-scripts', PLUGINS . 'scrollbar/jquery.mCustomScrollbar.concat.min.js', array('jquery'), 1.0, true);
}

add_action('wp_enqueue_scripts', 'scriptsInit');

function admin_style(){

    $cm_settings['codeEditor'] = wp_enqueue_code_editor(array('type' => 'text/css'));
    wp_localize_script('jquery', 'cm_settings', $cm_settings);

    wp_enqueue_script('wp-theme-plugin-editor');
    wp_enqueue_style('wp-codemirror');

    wp_enqueue_script('admin-custom', SCRIPTS . 'admin.js', array('jquery'), 1.0, false);
    wp_enqueue_style('admin-styles', STYLES . 'admin.css');
}

add_action('admin_enqueue_scripts', 'admin_style');

function registerMenus()
{
    register_nav_menus(
        array(
            'footer-menu' => __('Footer Menu', TEXTDOMAIN),
            'header-menu' => __('Header Menu', TEXTDOMAIN),
            'footer-links-menu-1' => __('Footer Links Menu 1', TEXTDOMAIN),
			'footer-links-menu-2' => __('Footer Links Menu 2', TEXTDOMAIN),
        )
    );
}

add_action('init', 'registerMenus');

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'הגדרות נוספות',
        'menu_title' => 'הגדרות נוספות',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
}

add_theme_support( 'post-thumbnails' );

add_filter('acf/settings/save_json', 'acf_json_save_point');
//acf json init
function acf_json_save_point( $path ) {
    $path = get_stylesheet_directory() . '/acf';
    return $path;
}


add_filter('acf/settings/load_json', 'acf_json_load_point');
function acf_json_load_point( $paths ) {
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/acf';
    return $paths;
}

//WOO
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_review_before', 'woocommerce_review_display_gravatar', 10);
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);

//add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 25);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 8);

add_action('add_meta_boxes', 'add_metabox_to_product');
add_action('woocommerce_single_title_custom', 'woocommerce_template_single_title');


function add_metabox_to_product(){
	add_meta_box(   'product_video', 'קישור לסרטון',  'woo_product_video', 'product', 'side', 'low');
}

function woo_product_video($post) {
	$yt_link = get_post_meta($post->ID, '_product_video', TRUE);

	echo '<input type="hidden" name="gallery_type_noncename" id="gallery_type_noncename" value="'.wp_create_nonce( 'product'. $post->ID ) . '" />';
	echo '<input type="text" style="width: 100%;" name="product_video" id="product_video" value="'.$yt_link.'">';

	$yt_id = null;
	if($yt_link){
		$yt_id = getYoutubeId($yt_link);
	}
	if($yt_id){
		echo '<iframe style="width: 100%; height: 300px; margin-top: 10px;" src="https://www.youtube.com/embed/'.$yt_id.'" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
	}
}


// Add to admin_init function
add_action('save_post', 'save_gallery_data' );

function save_gallery_data($post_id) {
	// verify this came from the our screen and with proper authorization.
	if ( !wp_verify_nonce( $_POST['gallery_type_noncename'], 'product'.$post_id )) {
		return $post_id;
	}

	// verify if this is an auto save routine. If it is our form has not been submitted, so we dont want to do anything
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
		return $post_id;

	// Check permissions
	if ( !current_user_can( 'edit_post', $post_id ) )
		return $post_id;


	// OK, we're authenticated: we need to find and save the data
	$post = get_post($post_id);
	if ($post->post_type == 'product') {
		update_post_meta($post_id, '_product_video', esc_attr($_POST['product_video']) );
		return(esc_attr($_POST['product_video']));
	}
	return $post_id;
}

//add_filter( 'comment_post_redirect', 'redirect_after_comment', 10, 2 );
//function redirect_after_comment($location, $comment ){
//	return add_query_arg('action', 'cmtpnd', $location);
//}


function custom_array_unique($array, $keep_key_assoc = false){
	$duplicate_keys = array();
	$tmp = array();

	foreach ($array as $key => $val){
		if (is_object($val))
			$val = (array)$val;

		if (!in_array($val, $tmp))
			$tmp[] = $val;
		else
			$duplicate_keys[] = $key;
	}

	foreach ($duplicate_keys as $key)
		unset($array[$key]);

	return $keep_key_assoc ? $array : array_values($array);
}
/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 6 );

function new_loop_shop_per_page( $cols ) {
	// $cols contains the current number of products per page based on the value stored on Options –> Reading
	// Return the number of products you wanna show per page.
	$cols = 6;
	return $cols;
}




require_once "inc/options.php";
require_once "inc/custom-types.php";
require_once "inc/users.php";


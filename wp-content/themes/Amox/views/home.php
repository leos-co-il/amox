<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
$logo = opt('logo_dark');
if ($fields['home_video_file']) : ?>
	<section class="video-home">
		<video id="video-home">
			<source src="<?= $fields['home_video_file']['url']; ?>" type="video/mp4">
		</video>
		<span id="video-button" class="button-home">
			<img src="<?= ICONS ?>play.png" alt="play-video">
		</span>
	</section>
<?php endif;
if ($fields['h_about_text'] || $fields['h_about_link']) : ?>
	<section class="home-about-block m-50 pb-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-lg-11 col-12">
					<div class="logo-title-wrap">
						<h2 class="block-title">
							<?= $fields['h_about_title'] ? $fields['h_about_title'] :
									lang_text(['he' => 'בואו לקרוא וללמוד עוד על', 'en' => 'Come read and learn more about'], 'he'); ?>
						</h2>
						<?php if ($logo) : ?>
							<img src="<?= $logo['url'] ?>" alt="logo" class="logo-title">
						<?php endif; ?>
					</div>
				</div>
				<?php if ($fields['h_about_text']) : ?>
					<div class="col-xl-10 col-lg-11 col-12">
						<div class="base-output text-center">
							<?= $fields['h_about_text']; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($fields['h_about_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a class="block-link" href="<?= $fields['h_about_link']['url']; ?>">
							<?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title'])
									? $fields['h_about_link']['title'] : lang_text(['he' => 'להמשך קריאה', 'en' => 'Continue reading'], 'he'); ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; if ($fields['h_products_1']) {
	get_template_part('views/partials/content', 'products_output',
			[
					'block_title' => $fields['h_prod_title_1'],
					'products' => $fields['h_products_1'],
					'block_link' => $fields['h_prod_link_1']
			]);
}
if ($fields['offer_title'] || $fields['offer_text'] || $fields['offer_link']) : ?>
	<section class="offer-block" <?php if ($fields['offer_img']) : ?>
		style="background-image: url('<?= $fields['offer_img']['url']; ?>')"
	<?php endif; ?>>
		<img src="<?= $fields['offer_img_1']['url']; ?>" class="offer-img img-off-right" alt="image-product">
		<img src="<?= $fields['offer_img_2']['url']; ?>" class="offer-img img-off-left" alt="image-product">
		<div class="offer-block-overlay"></div>
		<div class="offer-block-info">
			<div class="container">
				<div class="row justify-content-center">
					<?php if ($fields['offer_img_1']) : ?>
						<div class="col-lg-3 hide-stuff">
						</div>
					<?php endif; ?>
					<div class="col-lg col-12">
						<?php if ($fields['offer_title']) : ?>
							<h3 class="offer-title"><?= $fields['offer_title']; ?></h3>
						<?php endif;
						if ($fields['offer_text']) : ?>
							<p class="offer-subtitle"><?= $fields['offer_text']; ?></p>
						<?php endif; ?>
					</div>
					<?php if ($fields['offer_img_2']) : ?>
						<div class="col-lg-3 hide-stuff">
						</div>
					<?php endif; ?>
				</div>
				<?php if ($fields['offer_link']) : ?>
					<div class="row justify-content-center">
						<div class="col-auto">
							<a class="base-link" href="<?= $fields['offer_link']['url']; ?>">
								<?= (isset($fields['offer_link']['title']) && $fields['offer_link']['title'])
										? $fields['offer_link']['title'] :
										lang_text(['he' => 'עברו לקנייה', 'en' => 'Go shopping'], 'he'); ?>
							</a>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_benefit_item'] || $fields['h_benefits_text']) : ?>
	<section class="block-benefits">
		<?php if ($fields['h_benefits_title'] || $fields['h_benefits_text']) : ?>
			<div class="container">
				<?php if ($fields['h_benefits_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="logo-title-wrap">
							<h2 class="block-title">
								<?= $fields['h_benefits_title']; ?>
							</h2>
							<?php if ($logo) : ?>
								<img src="<?= $logo['url'] ?>" alt="logo" class="logo-title">
							<?php endif; ?>
						</div>
					</div>
				</div>
				<?php endif;
				if ($fields['h_benefits_text']) : ?>
					<div class="row justify-content-center align-items-center">
						<?php if ($fields['h_benefits_img_1']) : ?>
							<div class="col-lg-3 hide-stuff">
								<img src="<?= $fields['h_benefits_img_1']['url']; ?>">
							</div>
						<?php endif; ?>
						<div class="col">
							<div class="base-output text-center ul-centered">
								<?= $fields['h_benefits_text']; ?>
							</div>
						</div>
						<?php if ($fields['h_benefits_img_2']) : ?>
							<div class="col-lg-3 hide-stuff">
								<img src="<?= $fields['h_benefits_img_2']['url']; ?>">
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endif;
		if ($fields['h_benefit_item']) : ?>
		<div class="container">
			<div class="row justify-content-center align-items-start">
				<?php foreach ($fields['h_benefit_item'] as $i => $benefit) : ?>
					<div class="col-lg-4 col-md-6 col-12 mb-4 wow fadeInUpBig" data-wow-delay="0.<?= $i + 1; ?>s">
						<div class="benefit-item">
							<div class="ben-icon">
								<?php if ($benefit['ben_icon']) : ?>
									<img src="<?= $benefit['ben_icon']['url']; ?>">
								<?php endif; ?>
							</div>
							<h4 class="middle-title mb-3">
								<?= $benefit['ben_title']; ?>
							</h4>
							<p class="base-text text-center">
								<?= $benefit['ben_text']; ?>
							</p>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		<?php endif; ?>
	</section>
<?php endif;
if ($fields['home_cats']) {
	get_template_part('views/partials/content', 'cats_output',
			[
					'cats' => $fields['home_cats'],
			]);
}
if ($fields['h_video_items'] || $fields['h_video_text']) : ?>
	<div class="videos-col">
		<div class="container">
			<?php if ($fields['h_video_text']) : ?>
				<div class="row justify-content-center">
					<div class="col-xl-7 col-lg-9 col-sm-11 col-12">
						<div class="base-output text-center">
							<?= $fields['h_video_text']; ?>
						</div>
					</div>
				</div>
			<?php endif;
			if ($fields['h_video_items']) : ?>
				<div class="row justify-content-center align-items-stretch">
					<div class="col-xl-6 col-lg-8 col-sm-10 col-12 arrows-slider">
						<div class="video-slider" dir="rtl">
							<?php foreach ($fields['h_video_items'] as $video) : ?>
								<div>
									<div class="video-item" style="background-image: url('<?= getYoutubeThumb($video['h_video_link'])?>')">
										<span class="play-video" data-video="<?= getYoutubeId($video['h_video_link']); ?>">
											<img src="<?= ICONS ?>play.png">
										</span>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif;
if ($fields['h_posts']) : ?>
	<section class="posts-output m-50">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="logo-title-wrap">
						<h2 class="block-title">
							<?= $fields['h_posts_title'] ? $fields['h_posts_title'] :
							lang_text(['he' => 'בואו לקרוא וללמוד עוד על', 'en' => 'Come read and learn more about'], 'he'); ?>
						</h2>
						<?php if ($logo) : ?>
							<img src="<?= $logo['url'] ?>" alt="logo" class="logo-title">
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="home-posts-output">
			<?php foreach ($fields['h_posts'] as $post_item) : ?>
				<div class="home-post">
					<div class="home-post-img"
							<?php if (has_post_thumbnail($post_item)) : ?>
								style="background-image: url('<?= postThumb($post_item); ?>')"
							<?php endif;?>>
					</div>
					<div class="home-post-content">
						<div class="post-item-content">
							<h3 class="middle-title mb-3"><?= $post_item->post_title; ?></h3>
							<p class="base-text-small text-center mb-3">
								<?= text_preview($post_item->post_content, 30); ?>
							</p>
						</div>
						<a href="<?php the_permalink($post_item); ?>" class="block-link">
							<?= lang_text(['he' => 'להמשך קריאה', 'en' => 'Continue reading'], 'he'); ?>
						</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<?php if ($fields['h_posts_link']) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto mt-3">
						<a class="base-link" href="<?= $fields['h_posts_link']['url']; ?>">
							<?= (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title'])
									? $fields['h_posts_link']['title'] : lang_text(['he' => 'לכל המאמרים', 'en' => 'To all articles'], 'he'); ?>
						</a>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
if ($fields['home_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'content' => $fields['home_slider_seo'],
					'img' => $fields['home_slider_img'],
			]);
}
if ($fields['h_products_2']) {
	get_template_part('views/partials/content', 'products_output',
			[
					'block_title' => $fields['h_prod_title_2'],
					'products' => $fields['h_products_2'],
					'block_link' => $fields['h_prod_link_2']
			]);
}
get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) : ?>
	<div class="blue-slider">
		<?php get_template_part('views/partials/content', 'slider',
			[
					'content' => $fields['single_slider_seo'],
					'img' => $fields['slider_img'],
			]); ?>
	</div>
<?php endif; ?>
<?php if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'block_text' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>

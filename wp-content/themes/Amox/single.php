<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body">
	<div class="title-wrap">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-auto">
					<h1 class="block-title"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-11 col-12">
				<div class="row justify-content-center align-items-start mb-3">
					<div class="col-xl col-12 breadcrumbs-custom">
						<?php if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
						} ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container py-5">
		<div class="row justify-content-between">
			<div class="col-xl-6 col-lg-7 col-12">
				<div class="base-output post-output-line">
					<?php the_content(); ?>
				</div>
				<div class="socials-share">
					<span class="base-text text-center">
						 <?= lang_text(['he' => 'שתף', 'en' => 'Share'], 'he'); ?>
					</span>
					<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.2s">
						<img src="<?= ICONS ?>share-facebook.png">
					</a>
					<!--	WHATSAPP-->
					<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.4s">
						<img src="<?= ICONS ?>share-whatsapp.png">
					</a>
					<!--	MAIL-->
					<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.6s">
						<img src="<?= ICONS ?>share-mail.png">
					</a>
				</div>
			</div>
			<div class="col-xl-5 col-lg-5 col-12">
				<?php if (has_post_thumbnail()) : ?>
					<img src="<?= postThumb(); ?>" alt="post-image" class="post-page-img w-100">
				<?php endif; ?>
				<div class="granit-back post-form-wrap">
					<?php if ($f_title = opt('post_form_title')) : ?>
						<h2 class="base-title mb-2"><?= $f_title; ?></h2>
					<?php endif;
					if ($f_text = opt('post_form_text')) : ?>
						<p class="base-text text-center mb-3"><?= $f_text; ?></p>
					<?php endif; ?>
					<?php getForm('275'); ?>
				</div>
			</div>
		</div>
	</div>
</article>






<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
		'posts_per_page' => 3,
		'post_type' => 'post',
		'post__not_in' => array($postId),
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $post_terms,
				],
		],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 3,
			'orderby' => 'rand',
			'post_type' => 'post',
			'post__not_in' => array($postId),
	]);
}
$same_title = lang_text(['he' => 'מאמרים נוספים שעשויים לעניין אותך', 'en' => 'More articles that might interest you'], 'he');
if ($samePosts) : ?>
	<section class="same-posts">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="block-title mb-4">
						<?= $fields['same_title'] ?? $same_title; ?>
					</div>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $post) {
					get_template_part('views/partials/card', 'post',
							[
									'post' => $post,
							]);
				} ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) : ?>
	<div class="blue-slider">
		<?php get_template_part('views/partials/content', 'slider',
				[
						'content' => $fields['single_slider_seo'],
						'img' => $fields['slider_img'],
				]); ?>
	</div>
<?php endif; ?>
<?php if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'block_text' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>

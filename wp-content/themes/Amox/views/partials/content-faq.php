<?php if (isset($args['faq']) && $args['faq']) : ?>
	<div class="faq">
		<div class="container">
			<?php if ($logo = opt('logo_dark')) : ?>
				<div class="row justify-content-center">
					<div class="col-xl-2 col-lg-3 col-md-5 col-sm6 col-8">
						<a href="/" class="logo-faq">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="logo-title-wrap">
						<h2 class="block-title">
							<?= (isset($args['block_text']) && $args['block_text']) ? $args['block_text'] :
							lang_text(['he' => 'כל מה שרציתם לדעת -אנחנו עונים', 'en' => 'Everything you wanted to know - we answer'], 'he'); ?>
						</h2>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-xl-8 col-lg-9 col-md-11 col-12">
					<div id="accordion">
						<?php foreach ($args['faq'] as $num => $item) : ?>
							<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
								<div class="question-header" id="heading_<?= $num; ?>">
									<button class="question-title" data-toggle="collapse"
											data-target="#faqChild<?= $num; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<span class="faq-icon">
											?
										</span>
										<span class="faq-body-title"><?= $item['faq_question']; ?></span>
										<img src="<?= ICONS ?>faq-arrow.png" class="arrow-top" alt="arrow-top">
									</button>
									<div id="faqChild<?= $num; ?>" class="collapse faq-item answer-body"
										 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
										<div class="base-output small-output">
											<?= $item['faq_answer']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

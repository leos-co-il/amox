<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;


if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$post_id = $product->get_id();
$category = get_the_terms($post_id, 'product_cat');
$post_link = get_the_permalink();

$product_thumb = get_webp_img(get_post_thumbnail_id($post_id), 'large', '');
$product_gallery = $product->get_gallery_image_ids();
$product_gallery_images = [];
if($product_gallery){
	foreach ($product_gallery as $_item){
		$product_gallery_images[] = get_webp_img($_item, 'large', '');
	}
}
$product_video =  get_post_meta($post_id, '_product_video', true );
$fields = get_fields();
$info_del = $fields['product_delivery_info'] ?? opt('delivery_info');
$related_products = $fields['relates_products'];?>
<div class="title-wrap product-top mb-2">
	<div class="container">
		<div class="row justify-content-center align-items-center">
			<div class="col-auto">
				<?php do_action('woocommerce_single_title_custom'); ?>
			</div>
		</div>
	</div>
</div>
<?php
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="single-product-breadcrumbs">
					<a href="/"><?= lang_text(['he' => 'דף הבית', 'en' => 'Homepage'], 'he'); ?></a>>
					<?php if($category): ?>
						<a href="<?= get_term_link($category[0]) ?>">
							<?= $category[0]->name ?>
						</a>>
					<?php endif; ?>
					<span><?php the_title(); ?></span>
				</div>
			</div>
		</div>
	</div>
	<section class="product-summary">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-xl-4 col-lg-6 col-12">
					<?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					do_action( 'woocommerce_single_product_summary' );
					?>

					<?php if ($fields['product_more_info'] || $info_del): ?>
					<div class="product-item-info">
						<div id="accordion-product">
							<?php if ($fields['product_more_info']) : $c = 1; ?>
								<div class="card">
									<div class="card-header" id="heading<?= $c ?>">
										<h5 class="mb-0 accordion-button-wrap">
											<button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?= $c ?>" aria-expanded="true" aria-controls="collapse<?= $c ?>">
												<?= lang_text(['he' => 'מידע נוסף', 'en' => 'More info'], 'he'); ?>
											</button>
										</h5>
									</div>
									<div id="collapse<?= $c ?>" class="collapse" aria-labelledby="heading<?= $c ?>" data-parent="#accordion-product">
										<div class="card-body">
											<?= $fields['product_more_info']; ?>
										</div>
									</div>
								</div>
							<?php endif;
							if ($info_del) : $x = 2; ?>
								<div class="card">
									<div class="card-header" id="heading<?= $x ?>">
										<h5 class="mb-0 accordion-button-wrap">
											<button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?= $x ?>" aria-expanded="true" aria-controls="collapse<?= $x ?>">
												<?= lang_text(['he' => 'משלוחים והחזרות', 'en' => 'Shipments and returns'], 'he'); ?>
											</button>
										</h5>
									</div>
									<div id="collapse<?= $x ?>" class="collapse" aria-labelledby="heading<?= $x ?>" data-parent="#accordion-product">
										<div class="card-body base-output">
											<?= $info_del; ?>
										</div>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
					<?php endif; ?>
					<div class="socials-share">
					<span class="base-text text-center">
						 <?= lang_text(['he' => 'שתף', 'en' => 'Share'], 'he'); ?>
					</span>
						<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
						   class="social-share-link wow fadeInUp" data-wow-delay="0.2s">
							<img src="<?= ICONS ?>share-facebook.png">
						</a>
						<!--	WHATSAPP-->
						<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>"
						   class="social-share-link wow fadeInUp" data-wow-delay="0.4s">
							<img src="<?= ICONS ?>share-whatsapp.png">
						</a>
						<!--	MAIL-->
						<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
						   class="social-share-link wow fadeInUp" data-wow-delay="0.6s">
							<img src="<?= ICONS ?>share-mail.png">
						</a>
					</div>
				</div>
				<div class="col-lg-6 col-12 mt-lg-0 mt-4">
					<div class="product-gallery">
						<div class="gallery-slider" dir="rtl">
							<?php if($product_video): ?>
								<div class="slider-item" id="video-thumb" data-id="<?= getYoutubeId($product_video) ?>">
									<div id="video-holder" data-video="1">

									</div>
									<div class="play-overlay">
										<img src="<?= ICONS ?>play.png" alt="play-video">
									</div>
									<picture>
										<img src="<?= getYoutubeThumb($product_video) ?>" alt="video" class="w-100">
									</picture>
									<?php if ( $product->is_on_sale() ) : ?>

									<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . esc_html__( 'Sale!', 'woocommerce' ) . '</span>', $post, $product ); ?>

									<?php endif; ?>
								</div>
							<?php endif;
							if(has_post_thumbnail($post_id)): ?>
								<div class="slider-item">
									<?= $product_thumb ?>
								</div>
							<?php endif;
							foreach ($product_gallery_images as $img): ?>
								<div class="slider-item">
									<?= $img ?>
								</div>
							<?php endforeach; ?>
						</div>
						<div class="thumbs" dir="rtl">
							<?php if(has_post_thumbnail($post_id)): ?>
								<div class="thumb-item centered">
									<?= $product_thumb ?>
								</div>
							<?php endif;
							foreach ($product_gallery_images as $img): ?>
								<div class="thumb-item">
									<?= $img ?>
								</div>
							<?php endforeach;
							if($product_video): ?>
								<div class="thumb-item" id="video-thumb" data-id="<?= getYoutubeId($product_video) ?>">
									<div class="play-overlay">
										<img src="<?= ICONS ?>play.png" alt="play-video">
									</div>
									<picture>
										<img src="<?= getYoutubeThumb($product_video) ?>" alt="video">
									</picture>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	if($related_products): ?>
		<div class="related-pad">
			<div class="container mt-5">
				<div class="row">
					<div class="col-12">
						<h2 class="related-title">
							<?= lang_text(['he' => 'מוצרים נוספים שעשויים לעניין אותך', 'en' => 'Other products that may interest you'], 'he'); ?>
						</h2>
					</div>
					<div class="col-12 related-products-col mb-5 arrows-slider slider-prods">
						<div class="related-slider">
							<?php

							echo get_template_part('views/partials/product', 'slider', [
									'products' => $related_products,
							]);

							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
<div class="repeat-form-post">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'block_text' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
do_action( 'woocommerce_after_single_product' ); ?>

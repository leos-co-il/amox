(function($) {
	function generateYoutubeIframe(_id, _w, _h, _title, _clean, _autoplay){


		var params = {
			id: _id,
			width: _w,
			height: _h,
			title: _title,
			modestbranding: _clean ? 1 : 0,
			showinfo: _clean ? 0 : 1,
			autoplay: _autoplay ? 1: 0
		};

		return `<iframe width="${params.width}" height="${params.height}"

							src="https://www.youtube.com/embed/${params.id}
							?autoplay=${params.autoplay}&
							modestbranding=${params.modestbranding}&
							showinfo=${params.showinfo}&
							controls=0&
							loop=1&
							rel=0&
							playlist=${params.id}&
							mute=1"
							title="${params.title}"
							allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
					</iframe>`;

	}
	function addToCartAnimation(button){
		var target        = $('#mini-cart'),
			target_offset = target.offset();

		var target_x = target_offset.left,
			target_y = target_offset.top;

		var obj_id = 1 + Math.floor(Math.random() * 100000),
			obj_class = 'cart-animation-helper',
			obj_class_id = obj_class + '_' + obj_id;

		var obj = $("<div>", {
			'class': obj_class + ' ' + obj_class_id
		});

		button.parent().parent().append(obj);

		var obj_offset = obj.offset(),
			dist_x = target_x - obj_offset.left + 10,
			dist_y = target_y - obj_offset.top + 10,
			delay  = 0.8; // seconds

		setTimeout(function(){
			obj.css({
				'transition': 'transform ' + delay + 's ease-in',
				'transform' : "translateX(" + dist_x + "px)"
			});
			$('<style>.' + obj_class_id + ':after{ \
				transform: translateY(' + dist_y + 'px); \
				opacity: 1; \
				z-index: 99999999999; \
				border-radius: 100%; \
				height: 20px; \
				width: 20px; margin: 0; \
			}</style>').appendTo('head');
		}, 0);


		obj.show(1).delay((delay + 0.02) * 1000).hide(1, function() {
			$(obj).remove();
		});
	}
	function update_cart_count(){
		var count = $('#cart-count');
		count.html('<i class="far fa-smile-beam fa-spin" style="color: #fff"></i>');

		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'POST',
			data: {
				action: 'update_cart_count',
			},
			success: function (results) {
				count.html(results/10);
			}
		});

	}
	function play_video(){
		var id = $('#video-thumb').data('id');
		var pr_frame = generateYoutubeIframe(id, '', '', '', 1, 1);

		$('#video-holder').append(pr_frame);
	}
	function clear_frame(){
		$('#video-holder').empty();
	}
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	$( document ).ready(function() {
		$('.product-small-card .single_add_to_cart_button').click(function (e) {
			e.preventDefault();
			addToCartAnimation($(this));
			var cartEl = $('.mini-cart-wrap');
			cartEl.html('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');


			var btn = $(this);
			btn.addClass('loading');


			$(this).addClass('adding-cart');
			var product_id = $(this).data('id');
			var quantity = $('.qty-for-' + product_id).val();


			jQuery.ajax({
				url: '/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'add_product_to_cart',
					product_id: product_id,
					quantity: quantity,
				},

				success: function (results) {
					btn.removeClass('loading').addClass('clicked');
					cartEl.html(results);
					update_cart_count();
				}
			});

		});
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.toggleClass( 'show-menu' );
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.video-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			fade: true,
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-video').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="128%" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$(this).parent().html(frame);
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').children('.arrow-top').addClass('arrow-bottom');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').children('.arrow-top').removeClass('arrow-bottom');
		});
		var prodAcc = $('#accordion-product');
		prodAcc.on('shown.bs.collapse', function (e) {
			$('#accordion-product .collapse.show').parent().addClass('active');
		});

		prodAcc.on('hide.bs.collapse', function (e) {
			$('#accordion-product .collapse.show').parent().removeClass('active');
		});
		$('.product-slider-partial').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			rtl: true,
			dots: false,
			arrows: true,
			responsive: [
				{
					breakpoint: 1500,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$('.gallery-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			rtl: true,
			asNavFor: '.thumbs'
		});
		$('.thumbs').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			rtl: true,
			asNavFor: '.gallery-slider',
			dots: true,
			arrows: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		});
		$('.gallery-slider').on('afterChange', function(slick, currentSlide, nextSlide){

			var active = $('.gallery-slider .slick-current .slider-item').data('video');

			if(active){
				play_video();
			}else{
				clear_frame();
			}

		});
	});

	$(document).on('click', '.plus, .minus', function () {

		var $_class = $(this).hasClass('mini-cart-ctrl') ? '.mini-cart-qty-for-' : '.qty-for-';
		var qty =  $($_class + $(this).data('id'));
		var val = parseFloat(qty.val());
		var max = 9999;
		var min = 1;
		var step = 1;

		if ($(this).is('.plus')) {
			if (max && (max <= val)) {
				qty.val(max);
			} else {
				qty.val(val + step);
			}
		} else {
			if (min && (min >= val)) {
				qty.val(min);
			} else if (val > 1) {
				qty.val(val - step);
			}
		}
		//
		// if($(this).hasClass('mini-cart-ctrl')){
		// 	$('.preloader').addClass('now-loading');
		// 	updateMiniCart($(this).data('key'), qty.val())
		// }

	});
	$('.load-more-posts').click(function() {
		var termID = $(this).data('term');
		// var params = $('.take-json').html();
		var ids = '';
		var video = $(this).data('content');
		var page = $(this).data('page');

		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				// postType: postType,
				termID: termID,
				ids: ids,
				page: page,
				video: video,
				// taxType: taxType,
				action: 'get_more_function',
			},
			success: function (data) {
				if (!data.html) {
					$('.load-more-posts').addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
	//More products
	var button = $( '#loadmore a' ),
		paged = button.data( 'paged' ),
		maxPages = button.data( 'max_pages' );
	var textLoad = button.data('loading');
	var textLoadMore = button.data('load');

	button.click( function( event ) {

		event.preventDefault(); // предотвращаем клик по ссылке
		var ids = '';
		$('.more-prod').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		$.ajax({
			type : 'POST',
			dataType: 'json',
			url: '/wp-admin/admin-ajax.php',
			data : {
				paged : paged,
				ids: ids,
				action : 'loadmore'
			},
			beforeSend : function( xhr ) {
				button.text(textLoad);
			},
			success : function( data ){

				paged++;
				$('.put-here-prods').append(data.html);
				button.text(textLoadMore);

				if( paged === maxPages ) {
					button.remove();
				}
				if (!data.html) {
					$('.more-link').addClass('hide');
				}
			}

		});

	} );
})( jQuery );

var v = document.getElementById('video-home');
var tr = document.getElementById('video-button');
if (tr && v) {
	tr.addEventListener(
		'play',
		function() {
			v.play();
		},
		false);

	tr.onclick = function() {
		if (v.paused) {
			v.play();
			tr.classList.add('hide');
		} else {
			v.pause();
			tr.classList.remove('hide');
		}
		return false;
	};
}

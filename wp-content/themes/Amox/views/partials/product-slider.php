<?php

if(!isset($args['products']))
	return;

?>

<div class="product-slider-partial" dir="rtl">
	<?php

	foreach ($args['products'] as $_p){
		echo '<div class="slide-item p-3">';
		setup_postdata($GLOBALS['post'] =& $_p);
		wc_get_template_part( 'content', 'product' );
		echo '</div>';
	}
	wp_reset_postdata();

	?>
</div>
